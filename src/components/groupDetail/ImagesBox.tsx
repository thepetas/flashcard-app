import React from "react";
import {Grid} from "@material-ui/core";
import Image from "material-ui-image"

interface ImagesBoxProps {
    imagesUrl: string[];
    imageWidth: number;
}

const ImagesBox: React.FC<ImagesBoxProps> = ({imagesUrl, imageWidth}) => {
    return (
        <Grid container style={{height: '100%', width: '100%', textAlign: 'center'}} direction="row" justify="center">
            {imagesUrl.map(imageSrc => (
                <Grid item style={{width: imageWidth, height: '100%'}}>
                    <Image
                        src={imageSrc}
                        style={{
                            width: '100%',
                            height: '100%',
                            paddingTop: undefined,
                            textAlign: 'center'
                        }}
                        imageStyle={{
                            maxWidth: imageWidth,
                            maxHeight: '100%',
                            height: 'auto',
                            width: undefined,
                            position: 'inherit',
                            objectFit: 'contain',
                        }}
                    />
                </Grid>
            ))}
        </Grid>
    );
}

export default ImagesBox;
