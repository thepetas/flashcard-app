import React, {useState} from "react";
import FlashGroup from "../../data/FlashGroup";
import WindowUtils from "../../utils/WindowUtils";
import BottomControlArea from "./BottomControlArea";
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import ImagesBox from "./ImagesBox";
import TopInfoArea from "./TopInfoArea";

const useStyles = makeStyles(() => ({
    root: {
        width: '100%',
        height: '100%'
    },
    imagePath: {
        flexGrow: 1,
        backgroundColor: 'yellow'
    }
}));

interface DashboardProps {
    group: FlashGroup;
}

const createImageUrl = (groupId: string, imageName: string): string => process.env.PUBLIC_URL + '/' + groupId + '/' + imageName;


const GroupDetail: React.FC<DashboardProps> = ({group}) => {
    const classes = useStyles();
    const [currentIndex, setCurrentIndex] = useState(0);

    const currentFlashCard = group.cards[currentIndex];
    const imagesUrl = currentFlashCard.imageNames.map(src => createImageUrl(group.id, src));

    const canGoToNext = currentIndex < group.cards.length - 1;
    const canGoToPrevious = currentIndex > 0;

    const handleNextClick = (): void => {
        if (canGoToNext) {
            setCurrentIndex(currentIndex + 1);
        }
    };

    const handlePreviousClick = (): void => {
        if (canGoToPrevious) {
            setCurrentIndex(currentIndex - 1);
        }
    };

    const oneImageWidth = WindowUtils.getWidth() * 0.8 / imagesUrl.length;

    return (
        <Grid container direction="column" className={classes.root} spacing={2}>
            <Grid item>
                <TopInfoArea name={group.name} currentIndex={currentIndex} totalItems={group.cards.length} />
            </Grid>
            <Grid item style={{height: WindowUtils.getHeight() - 300, width: '100%'}}>
                <ImagesBox imagesUrl={imagesUrl} imageWidth={oneImageWidth} />
            </Grid>
            <Grid item>
                <BottomControlArea
                    answer={currentFlashCard.answer}
                    nextDisabled={!canGoToNext}
                    previousDisabled={!canGoToPrevious}
                    onPreviousClick={handlePreviousClick}
                    onNextClick={handleNextClick}
                />
            </Grid>
        </Grid>
    );
}

export default GroupDetail;