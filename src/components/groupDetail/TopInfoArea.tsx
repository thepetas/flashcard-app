import React from "react";
import {Grid, Typography} from "@material-ui/core";

interface TopInfoAreaProps {
    name: string;
    currentIndex: number;
    totalItems: number;
}

const TopInfoArea: React.FC<TopInfoAreaProps> = ({name, totalItems, currentIndex}) => {
    return (
        <Grid container spacing={3} justify="center">
            <Grid item>
                <Typography variant="h5">
                    {name} - {currentIndex + 1} z {totalItems}
                </Typography>
            </Grid>
        </Grid>
    );
}

export default TopInfoArea;