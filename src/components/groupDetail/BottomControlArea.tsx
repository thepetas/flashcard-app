import React, {useEffect, useState} from "react";
import {Button, Grid, Typography} from "@material-ui/core";

interface BottomControlAreaProps {
    onPreviousClick: () => void;
    onNextClick: () => void;
    previousDisabled: boolean;
    nextDisabled: boolean;
    answer: string;
}

const BottomControlArea: React.FC<BottomControlAreaProps> = ({
                                                                 answer,
                                                                 nextDisabled,
                                                                 previousDisabled,
                                                                 onNextClick,
                                                                 onPreviousClick
                                                             }) => {
    const [show, setShow] = useState(false);

    useEffect(() => {
        setShow(false);
    }, [answer])

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container spacing={3} justify="center">
                <Grid item>
                    <Button disabled={previousDisabled} variant="contained" onClick={onPreviousClick}>Předchozí</Button>
                </Grid>
                <Grid item>
                    <Button
                        disabled={show}
                        variant="contained"
                        color="primary"
                        onClick={() => setShow(true)}
                    >
                        Zobrazit, co je na obrázku
                    </Button>
                </Grid>
                <Grid item>
                    <Button disabled={nextDisabled} variant="contained" onClick={onNextClick}>Další</Button>
                </Grid>
            </Grid>
            <Grid item container justify="center">
                <Grid item>
                    {show && (
                        <Typography variant="h5">{answer}</Typography>
                    )}
                </Grid>
            </Grid>
        </Grid>
    )
}

export default BottomControlArea;