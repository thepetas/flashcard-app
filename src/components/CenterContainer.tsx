import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    width: '100%',
    height: '100%'
  },
}));

const CenterContainer: React.FC = ({children}) => {
  const classes = useStyles();

  return (
    <Grid container className={classes.root} direction="column" justify="center">
      <Grid item container justify="center" direction="column" alignItems="center">
        {children}
      </Grid>
    </Grid>
  );

};

export default CenterContainer;
