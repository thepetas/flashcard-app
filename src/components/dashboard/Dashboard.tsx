import React from "react";
import FlashGroup from "../../data/FlashGroup";
import MaterialTable from "material-table";
import {Button, Grid} from "@material-ui/core";
import {tableIcons} from "./tableIcons";
import { useHistory } from "react-router-dom";
import routes from "../../routes";

interface DashboardProps {
    groups: FlashGroup[];
}

const Dashboard: React.FC<DashboardProps> = ({groups}) => {
    const history = useHistory();

    return (
        <Grid container justify="center" alignItems="center">
            <Grid item container style={{width: '60%'}}>
                <MaterialTable
                    title="Seznam kartiček"
                    data={groups}
                    columns={[
                        {
                            type: "string",
                            field: "name",
                            title: "Název kartiček"
                        },
                        // {
                        //     type: "string",
                        //     title: "Počet kart"
                        // },
                    ]}
                    actions={[
                        {
                            icon: () => <Button variant="contained">Spustit</Button>,
                            tooltip: 'Spustit',
                            onClick: (event: any, group: FlashGroup | FlashGroup[]) => history.push(routes.flashGroupDetail((group as FlashGroup).id))
                        }
                    ]}
                    style={{
                        width: '100%'
                    }}
                    options={{
                        search: false,
                        paging: false,
                        actionsColumnIndex: -1
                    }}
                    localization={{
                        header: {
                            actions: 'Akce'
                        }
                    }}
                    icons={tableIcons}
                />

            </Grid>
        </Grid>

    );

}

export default Dashboard