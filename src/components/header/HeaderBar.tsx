import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import {makeStyles, Theme} from "@material-ui/core/styles";
import SchoolIcon from '@material-ui/icons/School';
import {useHistory} from "react-router-dom";
import routes from "../../routes";
import {Box} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
    topText: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        cursor: 'pointer'
    },
    spacer: {
        flexGrow: 1
    }
}));

export const toolbarSpacerId = "spacerBoxId";

const HeaderBar: React.FC = () => {
    const classes = useStyles();
    const history = useHistory();

    return (
        <AppBar position="sticky">
            <Toolbar variant="dense">
                <SchoolIcon />
                <Typography
                    variant="h6"
                    color="inherit"
                    className={classes.topText}
                    onClick={() => history.push(routes.dashboard)}
                >
                    Online kartičky
                </Typography>
                <Box className={classes.spacer} id={toolbarSpacerId} />
            </Toolbar>
        </AppBar>
    );
}

export default HeaderBar;
