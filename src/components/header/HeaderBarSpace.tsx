import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { toolbarSpacerId } from "./HeaderBar";


/**
 * Komponenta slouží pro vložení vlastního obsahu do horní lišty aplikace pomocí.
 * Obsah, který má být do lišty vložený stačí obalit touto komponentou.
 */
const HeaderBarSpace: React.FC = ({children}) => {

  const [element, setElement] = useState<HTMLElement | null>(null);

  useEffect(() => {
    setElement(document.getElementById(toolbarSpacerId));
  }, [setElement]);

  if (element != null) {
    return ReactDOM.createPortal(
      children,
      element
    );
  } else {
    return null;
  }
};

export default HeaderBarSpace;
