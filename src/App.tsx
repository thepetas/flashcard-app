import React from 'react';
import {makeStyles, Theme} from "@material-ui/core/styles";
import Dashboard from "./components/dashboard/Dashboard";
import {Redirect, Route, Switch as SwitchRouter} from "react-router-dom"
import FlashGroup from "./data/FlashGroup";
import GroupDetail from "./components/groupDetail/GroupDetail";
import routes from "./routes";
import HeaderBar from "./components/header/HeaderBar";
import {Box, Grid, Typography} from "@material-ui/core";
import _ from 'lodash';
import HeaderBarSpace from "./components/header/HeaderBarSpace";
import forestJson from './data/forest.json';
import fieldJson from './data/field.json';
import urbanWildlife from './data/urbanWildlife.json';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        height: 'calc(100vh - 64px)',
        width: '100%',
        flexGrow: 1
    },
    pageName: {
        marginLeft: theme.spacing(2)
    }
}));

const shuffleCardsInGroup = ({id, name, cards}: FlashGroup): FlashGroup => ({
    id,
    name,
    cards: _.shuffle([...cards]),
})

const renderRoutes = (groups: FlashGroup[]) => {
    const groupRoutes = groups.map(g => (
        <Route exact path={routes.flashGroupDetail(g.id)} key={g.id}>
            <>
                <HeaderBarSpace>
                    <Typography style={{marginLeft: '20px'}}>
                        {g.name}
                    </Typography>
                </HeaderBarSpace>
                <GroupDetail group={g} />
            </>
        </Route>
    ));

    const dashboardRoute = (
        <Route exact path={routes.dashboard} key="dasbhboard">
            <Dashboard groups={groups} />
        </Route>
    );

    const otherRedirectRoute = (
        <Route exact path="*" key="redirect">
            <Redirect to={routes.dashboard} />
        </Route>
    );

    return [...groupRoutes, dashboardRoute, otherRedirectRoute];
}

const App: React.FC = () => {
    const classes = useStyles();

    const groups = [
        forestJson,
        fieldJson,
        urbanWildlife
    ].map(shuffleCardsInGroup);

    return (
        <>
            <HeaderBar />
            <Grid container direction="column" className={classes.root}>
                <Box mx={6} mt={4} style={{flexGrow: 1}}>
                    <SwitchRouter>
                        {renderRoutes(groups)}
                    </SwitchRouter>
                </Box>
            </Grid>
        </>
    );
}

export default App;
