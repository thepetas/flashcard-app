export default class WindowUtils {

    static getWidth() {
        return window.innerWidth
    }

    static getHeight() {
        return window.innerHeight;
    }
}