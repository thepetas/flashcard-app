const routes = {
    dashboard: '/',
    flashGroupDetail: (id: string) => `/skupina/${id}`
}

export default routes;
