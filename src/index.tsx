import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {createMuiTheme, ThemeProvider} from '@material-ui/core';
import {HashRouter} from "react-router-dom";

const myTheme = createMuiTheme({
    palette: {
        primary: {
            main: '#bd3632'
        }
    }
});

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={myTheme}>
            <HashRouter>
                <App />
            </HashRouter>
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
