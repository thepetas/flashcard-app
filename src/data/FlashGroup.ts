import Flashcard from "./FlashCard";

export default interface FlashGroup {
    id: string;
    name: string;
    cards: Flashcard[];
}