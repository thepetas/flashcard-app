export default interface Flashcard {
    id: number;
    answer: string;
    imageNames: string[];
}
